# change log

## version 0.3.3 2023-08-04

* fix starttls behaviour with Python 3.11

## version 0.3.2 2021-03-07

* remove broken CA certificate statistics

## version 0.3.1 2019-06-23

* move to new project location

## version 0.3.0 2016-06-18

* add alternative --hostname parameter to make check_xmpp compatible with
  check_v46 (thanks to Andreas Krause for the idea)

## version 0.2.1 2016-01-23

* fix perfdata output for daysvalid metric

## version 0.2 2015-12-29

* improve human readable output by returning both response time as well as
  certificate expiry

## version 0.1.2 2015-02-11

* first icinga exchange release
* convert documentation to markdown
* add icingaexchange.yml

## version 0.1.1 2015-02-11

* rename to xmpp_checkng to avoid clash with existing xmpp_check on Icinga
  exchange
* add proper documentation

## version 0.1 2015-02-11

* support S2S and C2S checks
* support STARTTLS handling
* support certificate validity checks
